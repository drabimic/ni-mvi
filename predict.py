import argparse
import cv2
import numpy as np
import os

import models


def predict_frame(model, frame_pair):
    pred = model(np.expand_dims(np.transpose((np.concatenate(
        (frame_pair[0], frame_pair[1]), axis=2).astype("float") - 172.5)/172.5, (1, 0, 2)), axis=0), training=False)

    return np.transpose(((pred[0].numpy() * 172.5) + 172.5).astype("uint8"), (1, 0, 2))


def load_image(path):
    if not os.path.exists(path):
        raise RuntimeError(f"{path} doesn't exist.")

    img = cv2.imread(path)
    return cv2.resize(
        img, (models.constants.VIDEO_WIDTH, models.constants.VIDEO_HEIGHT), interpolation=cv2.INTER_AREA)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--first-path', required=True)
    parser.add_argument('--second-path', required=True)
    parser.add_argument('--output-path', required=True)
    parser.add_argument('--model', required=True,
                        choices=[model.name for model in models.ModelTypes])
    parser.add_argument('--models-path', default="./checkpoints")

    args = vars(parser.parse_args())
    args["model"] = models.ModelTypes[args["model"]]

    print("Initializating model")
    model = models.initialize_model(
        args["model"], args["models_path"]).prediction_model()

    frame_pair = list()
    frame_pair.append(load_image(args["first_path"]))
    frame_pair.append(load_image(args["second_path"]))

    pred = predict_frame(model, frame_pair)
    cv2.imwrite(args["output_path"], pred)


if __name__ == "__main__":
    main()
