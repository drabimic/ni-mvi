import tensorflow as tf
from tensorflow.keras import models, losses

from models import constants


class VGGLoss:
    def __init__(self):
        vgg = tf.keras.applications.VGG19(include_top=False, input_shape=(
            constants.VIDEO_WIDTH, constants.VIDEO_HEIGHT, 3))

        self.model = models.Model(
            vgg.input, outputs=vgg.get_layer(name='block5_conv4').output)

    def _get_features(self, image):
        input = tf.identity(image)
        tf.keras.applications.vgg19.preprocess_input(
            (tf.identity(input) * 127.5) + 127.5)

        return self.model(input, training=False)

    def loss(self, real, predicted):
        return losses.MeanSquaredError()(self._get_features(real), self._get_features(predicted))
