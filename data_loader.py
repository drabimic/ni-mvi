import tensorflow as tf
import os
from random import shuffle

import models


def frame_key(frame_file):
    return int(os.path.splitext(frame_file)[0])


class DataLoader(object):
    def __init__(self, image_dir):
        self.frame_triplets = list()

        for video_idx in os.listdir(image_dir):
            video_root = os.path.join(image_dir, video_idx)
            if not os.path.isdir(video_root):
                raise RuntimeError("Dataset has an unexpected structure.")

            current_triplet = list()
            for frame in sorted(os.listdir(video_root), key=frame_key):
                path = os.path.join(video_root, frame)

                if len(current_triplet) < 3:
                    current_triplet.append(path)

                    if len(current_triplet) == 3:
                        self.frame_triplets.append(current_triplet)

                    continue

                # This should always be true, but just in case
                if frame_key(os.path.basename(current_triplet[2])) == frame_key(frame) - 1:
                    current_triplet = current_triplet[1:]
                    current_triplet.append(path)

                    self.frame_triplets.append(current_triplet)
                else:
                    current_triplet.clear()
                    current_triplet.append(path)

        shuffle(self.frame_triplets)

    def _triplet_generator(self):
        for triplet in self.frame_triplets:
            yield self._input_output_pair(self._parse_triplet(triplet))

    def _parse_triplet(self, path_triplet):
        def parse_image(path):
            image = tf.io.read_file(path)
            image = tf.image.decode_jpeg(image, channels=3)
            image = tf.image.convert_image_dtype(image, tf.uint8)
            image = tf.image.resize(
                image, [models.constants.VIDEO_WIDTH, models.constants.VIDEO_HEIGHT])

            return image

        return parse_image(path_triplet[0]), parse_image(path_triplet[1]), parse_image(path_triplet[2])

    def _input_output_pair(self, frame_triplet):
        # Normalize to [-1, 1] range
        return (tf.cast(tf.concat([frame_triplet[0], frame_triplet[2]], axis=2), tf.float32) - 127.5) / 127.5, (tf.cast(frame_triplet[1], tf.float32) - 127.5) / 127.5

    def dataset(self, batch_size, threads=4):
        dataset = tf.data.Dataset.from_generator(self._triplet_generator, output_signature=(
            tf.TensorSpec(shape=(models.constants.VIDEO_WIDTH,
                                 models.constants.VIDEO_HEIGHT, 6), dtype=tf.float32),
            tf.TensorSpec(shape=(models.constants.VIDEO_WIDTH,
                                 models.constants.VIDEO_HEIGHT, 3), dtype=tf.float32)
        ))

        # Batch the input, drop remainder to get a defined batch size.
        # Prefetch the data for optimal GPU utilization.
        dataset = dataset.shuffle(30).batch(
            batch_size, drop_remainder=True).prefetch(tf.data.experimental.AUTOTUNE)

        return dataset
