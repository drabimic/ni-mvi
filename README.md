# ni-mvi

## Introduction

Take short 5-10 seconds video and create a generator
for frame interpolation, which will generate high
frame rate slow motion. Use both architectures:
GAN and U-Net and compare the results

## Resources outside of this repostiory

Model: https://drive.google.com/drive/folders/1q2kcxmXCNw5oG3YZwdQPXDeqxvTF8uAU?usp=sharing

Dataset: https://drive.google.com/drive/folders/1UoacSNSUsO45WkOt01JUfcxl5ofYKoVd?usp=sharing

## Setup
Run

    pip install -r requirements.txt

to install python dependencies.

Main scripts are
 * train.py - train the model
 * predict.py - take two images and predict middle image
 * double_fps.py - take video and double its FPS

Input parameters of all the script should be self explanatory.
