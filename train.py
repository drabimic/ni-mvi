import argparse
import tensorflow as tf

import models
import data_loader

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    tf.config.experimental.set_memory_growth(gpus[0], True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--models-path', default="./checkpoints")
    parser.add_argument('--dataset-path', required=True)
    parser.add_argument('--model', required=True,
                        choices=[model.name for model in models.ModelTypes])
    parser.add_argument('--epochs', default=30)
    parser.add_argument('--batch-size', default=2)
    parser.add_argument('--batches-per-epochs', default=10)

    args = vars(parser.parse_args())
    args["model"] = models.ModelTypes[args["model"]]

    print("Initializating model")
    model = models.initialize_model(args["model"], args["models_path"], True)
    print("Initializating dataset")
    dataset = data_loader.DataLoader(
        args["dataset_path"]).dataset(int(args["batch_size"]))

    model.train(dataset, int(
        args["epochs"]), int(args["batches_per_epochs"]))


if __name__ == "__main__":
    main()
