import tensorflow as tf
import time
import os
from tensorflow.keras import layers, models, losses

from models import unet_model
from models import constants, model_base, vgg


class Generator(model_base.ModelBase):
    def __init__(self):
        super().__init__()

    @staticmethod
    def build_model():
        return unet_model.UNet.build_model()


class Discriminator(model_base.ModelBase):
    def __init__(self):
        super().__init__()

    @staticmethod
    def loss(real_sequence, fake_sequence):
        real_loss = losses.BinaryCrossentropy(from_logits=True)(tf.ones_like(
            real_sequence), real_sequence)
        fake_loss = losses.BinaryCrossentropy(from_logits=True)(tf.zeros_like(
            fake_sequence), fake_sequence)
        total_loss = real_loss + fake_loss
        return total_loss

    @staticmethod
    def conv_block(inputs, filter_count, filter_size, stride, dropout):
        conv = layers.Convolution2D(
            filter_count, (filter_size, filter_size), strides=(stride, stride), padding='same')(inputs)
        activation = layers.LeakyReLU()(conv)
        return layers.Dropout(dropout)(activation)

    @staticmethod
    def build_model():
        inputs = tf.keras.Input(
            shape=(constants.VIDEO_WIDTH, constants.VIDEO_HEIGHT, 9))

        block = Discriminator.conv_block(inputs, 32, 5, 2, 0.3)
        block = Discriminator.conv_block(block, 64, 5, 2, 0.3)
        block = Discriminator.conv_block(block, 128, 5, 2, 0.3)
        block = Discriminator.conv_block(block, 256, 5, 2, 0.3)

        flatten = layers.Flatten()(block)
        outputs = layers.Dense(1)(flatten)

        return models.Model(inputs=inputs, outputs=outputs)


class DCGAN:
    def __init__(self, models_root="./checkpoints"):
        self.generator = Generator()
        self.discriminator = Discriminator()
        self.vgg_loss = vgg.VGGLoss()
        self.pretrained = False
        self.models_root = models_root
        self.checkpoint_path = os.path.join(
            models_root, self.__class__.__name__)
        self.checkpoint = tf.train.Checkpoint(generator_optimizer=self.generator.optimizer,
                                              discriminator_optimizer=self.discriminator.optimizer,
                                              generator=self.generator.model,
                                              discriminator=self.discriminator.model)

    def load_model_training(self):
        try:
            self.checkpoint.read(self.checkpoint_path)
        except:
            pass
        else:
            self.pretrained = True

    def load_model(self):
        self.generator.load_model(self.models_root)
        self.discriminator.load_model(self.models_root)

    def save_model(self):
        self.generator.save_model(self.models_root)
        self.discriminator.save_model(self.models_root)

    def prediction_model(self):
        return self.generator.model

    @tf.function
    def pretrain_step(self, edge_frames, middle_frames):
        with tf.GradientTape() as tape:
            predicted = self.generator.model(edge_frames)
            mse_loss = losses.MeanSquaredError()(middle_frames, predicted)
            vgg_loss = self.vgg_loss.loss(middle_frames, predicted)
            loss = vgg_loss + mse_loss

            gradients = tape.gradient(
                loss, self.generator.model.trainable_variables)
            self.generator.optimizer.apply_gradients(
                zip(gradients, self.generator.model.trainable_variables))

        return mse_loss, vgg_loss

    def pretrain(self, dataset, batches_per_epoch):
        for epoch in range(5):
            for i, (x, y) in enumerate(dataset):
                mse_loss, vgg_loss = self.pretrain_step(x, y)

                if (i + 1) % 5 == 0:
                    print(
                        f'Epoch: {epoch + 1}, Batch: {i + 1}, Generator MSE loss: {mse_loss}, Generator VGG loss: {vgg_loss}')
                    self.checkpoint.write(self.checkpoint_path)

                if i >= batches_per_epoch:
                    break

        self.checkpoint.write(self.checkpoint_path)
        self.pretrained = True

    @ tf.function
    def train_step(self, edge_frames, middle_frames):
        first_frames, last_frames = tf.split(
            edge_frames, num_or_size_splits=2, axis=3)

        real_triplets = tf.concat([first_frames, middle_frames,
                                   last_frames], axis=3)

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            predicted = self.generator.model(edge_frames, training=True)

            predicted_tripleds = tf.concat([first_frames, predicted,
                                            last_frames], axis=3)

            real_output = self.discriminator.model(
                real_triplets, training=True)
            fake_output = self.discriminator.model(
                predicted_tripleds, training=True)

            g_adv_loss = losses.BinaryCrossentropy(from_logits=True)(
                tf.ones_like(fake_output), fake_output) * 1e-2
            g_mse_loss = losses.MeanSquaredError()(middle_frames, predicted)
            g_vgg_loss = self.vgg_loss.loss(middle_frames, predicted)

            gen_loss = g_adv_loss + g_mse_loss + g_vgg_loss
            disc_loss = self.discriminator.loss(real_output, fake_output)

        gradients_of_generator = gen_tape.gradient(
            gen_loss, self.generator.model.trainable_variables)
        gradients_of_discriminator = disc_tape.gradient(
            disc_loss, self.discriminator.model.trainable_variables)

        self.generator.optimizer.apply_gradients(
            zip(gradients_of_generator, self.generator.model.trainable_variables))
        self.discriminator.optimizer.apply_gradients(
            zip(gradients_of_discriminator, self.discriminator.model.trainable_variables))

        return g_adv_loss, g_mse_loss, g_vgg_loss, disc_loss

    def train(self, dataset, epochs, batches_per_epoch):
        if not self.pretrained:
            print("Starting pretrain")

            start = time.time()
            self.pretrain(dataset, batches_per_epoch)
            print('Time for pretrain is {} sec'.format(
                time.time()-start))

        print("Starting training")
        for epoch in range(epochs):
            start = time.time()

            for i, (edge_frames, middle_frames) in enumerate(dataset):
                g_adv_loss, g_mse_loss, g_vgg_loss, disc_loss = self.train_step(
                    edge_frames, middle_frames)

                if (i + 1) % 5 == 0:
                    print(
                        f'Epoch: {epoch + 1}, Batch: {i + 1}, Generator MSE loss: {g_mse_loss}, Generator VGG loss: {g_vgg_loss}, Generator ADV loss: {g_adv_loss},  Discriminator loss: {disc_loss}')
                    self.checkpoint.write(self.checkpoint_path)

                if i >= batches_per_epoch:
                    break

            print('Time for epoch {} is {} sec'.format(
                epoch + 1, time.time()-start))

            self.checkpoint.write(self.checkpoint_path)
        #self.save_model()
