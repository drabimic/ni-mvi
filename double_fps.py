import cv2
import numpy as np
import os
import argparse

import models
import predict


def double_fps(video_path, model):

    # Initialize input and get properties
    video = cv2.VideoCapture(video_path)
    # This can inaccurate - we streaming the data anyways
    frame_count = video.get(cv2.CAP_PROP_FRAME_COUNT)
    print(f"Original frame count: {frame_count}")
    new_fps = video.get(cv2.CAP_PROP_FPS)  # * 2

    height = models.constants.VIDEO_HEIGHT
    width = models.constants.VIDEO_WIDTH

    # Initialize output
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    name, ext = os.path.splitext(video_path)
    out = cv2.VideoWriter(f"{name}_{new_fps}fps{ext}",
                          fourcc, new_fps, (width, height))

    # Initialize frame buffer
    frame_pair = np.zeros(shape=(2, models.constants.VIDEO_HEIGHT,
                                 models.constants.VIDEO_WIDTH, 3), dtype="uint8")
    ret, frame = video.read()
    if not ret:
        return
    frame_pair[1] = cv2.resize(
        frame, (models.constants.VIDEO_WIDTH, models.constants.VIDEO_HEIGHT), interpolation=cv2.INTER_AREA)

    ret, frame = video.read()
    frame_number = 2
    while ret:
        frame_pair[0] = frame_pair[1]
        frame_pair[1] = cv2.resize(
            frame, (models.constants.VIDEO_WIDTH, models.constants.VIDEO_HEIGHT), interpolation=cv2.INTER_AREA)
        # Generate the middle frame
        pred = predict.predict_frame(model, frame_pair)

        # Write first and predicted frame
        out.write(frame_pair[0])
        out.write(pred)

        # Print progress
        print(f"Frame number: {frame_number}")
        if frame_number % int((frame_count / 10)) == 0:
            print("FPS doubling is {:.2f}% done.".format(
                frame_number / frame_count * 100))

        # Read next frame
        ret, frame = video.read()
        frame_number += 1

    # Write the last frame
    out.write(cv2.resize(
        frame_pair[1], (width, height), interpolation=cv2.INTER_AREA))

    # Release
    video.release()
    out.release()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', required=True)
    parser.add_argument('--model', required=True,
                        choices=[model.name for model in models.ModelTypes])
    parser.add_argument('--models-path', default="./checkpoints")

    args = vars(parser.parse_args())
    args["model"] = models.ModelTypes[args["model"]]

    print("Initializating model")
    model = models.initialize_model(
        args["model"], args["models_path"]).prediction_model()

    double_fps(args["path"],  model)


if __name__ == "__main__":
    main()
