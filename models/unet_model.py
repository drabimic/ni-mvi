import tensorflow as tf
import time
import os
from tensorflow.keras import layers, models, losses, optimizers

from models import constants, model_base, vgg


class UNet(model_base.ModelBase):
    @staticmethod
    def _convolution_block(inputs, filter_count, filter_size, pooling=True):
        conv = layers.Convolution2D(
            filter_count, (filter_size, filter_size), padding='same')(inputs)
        activation = layers.LeakyReLU(alpha=0.1)(conv)
        batch_norm = layers.BatchNormalization()(activation)
        conv = layers.Convolution2D(
            filter_count, (filter_size, filter_size), padding='same')(batch_norm)
        activation = layers.LeakyReLU(alpha=0.1)(conv)
        batch_norm = layers.BatchNormalization()(activation)

        output = conv
        if pooling:
            output = layers.MaxPooling2D(pool_size=(2, 2))(conv)

        return batch_norm, output

    @staticmethod
    def _deconvolution_block(inputs, merge, filter_count, filter_size):
        output = layers.Concatenate(
            axis=-1)([layers.UpSampling2D(size=(2, 2))(inputs), merge])

        output = layers.Convolution2D(
            filter_count, (filter_size, filter_size), padding='same')(output)
        output = layers.LeakyReLU(alpha=0.1)(output)
        output = layers.BatchNormalization()(output)
        output = layers.Convolution2D(
            filter_count, (filter_size, filter_size), padding='same')(output)
        output = layers.LeakyReLU(alpha=0.1)(output)
        return layers.BatchNormalization()(output)

    @staticmethod
    def build_model():
        # 2 images -> 6 layers
        inputs = tf.keras.Input(
            shape=(constants.VIDEO_WIDTH, constants.VIDEO_HEIGHT, 6))

        skip1, output = UNet._convolution_block(inputs, 32, 3)
        skip2, output = UNet._convolution_block(output, 64, 3)
        skip3, output = UNet._convolution_block(output, 128, 3)
        skip4, output = UNet._convolution_block(output, 256, 3)
        output = UNet._convolution_block(output, 512, 3, False)[1]

        output = UNet._deconvolution_block(output, skip4, 256, 3)
        output = UNet._deconvolution_block(output, skip3, 128, 3)
        output = UNet._deconvolution_block(output, skip2, 64, 3)
        output = UNet._deconvolution_block(output, skip1, 32, 3)

        outputs = layers.Convolution2D(3, 1, 1, activation="tanh")(output)

        return models.Model(inputs=inputs, outputs=outputs)

    def __init__(self, models_root="./checkpoints"):
        super().__init__()
        self.vgg_loss = vgg.VGGLoss()
        self.checkpoint = tf.train.Checkpoint(model=self.model,
                                              optimizer=self.optimizer)

        self.models_root = models_root
        self.checkpoint_path = os.path.join(
            models_root, self.__class__.__name__)

    def load_model_training(self):
        try:
            self.checkpoint.read(self.checkpoint_path)
        except:
            pass

    def load_model(self):
        super().load_model(self.models_root)

    def save_model(self):
        super().save_model(self.models_root)

    def prediction_model(self):
        return self.model

    def loss(self, real, predicted):
        return self.vgg_loss.loss(real, predicted) + losses.MeanSquaredError()(real, predicted)

    @tf.function
    def train_step(self, edge_frames, middle_frames):
        with tf.GradientTape() as tape:
            predicted = self.model(edge_frames)
            mse_loss = losses.MeanSquaredError()(middle_frames, predicted)
            vgg_loss = self.vgg_loss.loss(middle_frames, predicted)
            loss = vgg_loss + mse_loss

            gradients = tape.gradient(
                loss, self.model.trainable_variables)
            self.optimizer.apply_gradients(
                zip(gradients, self.model.trainable_variables))

        return mse_loss, vgg_loss

    def train(self, dataset, epochs, batches_per_epoch):
        print("Starting training")
        for epoch in range(epochs):
            start = time.time()

            for i, (edge_frames, middle_frames) in enumerate(dataset):
                mse_loss, vgg_loss = self.train_step(
                    edge_frames, middle_frames)

                if (i + 1) % 5 == 0:
                    print(
                        f'Epoch: {epoch + 1}, Batch: {i + 1}, MSE Loss: {mse_loss}, VGG Loss: {vgg_loss}')
                    self.checkpoint.write(self.checkpoint_path)

                if i >= batches_per_epoch:
                    break

            print('Time for epoch {} is {} sec'.format(
                epoch + 1, time.time()-start))

            self.checkpoint.write(self.checkpoint_path)
        #self.save_model()
