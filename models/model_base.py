from tensorflow.keras import models, optimizers
import os


class ModelBase:
    def __init__(self, optimizer=None):
        self.model = self.build_model()
        self.optimizer = optimizer

        if self.optimizer is None:
            self.optimizer = optimizers.Adam(learning_rate=1e-4, epsilon=1e-08)

    def load_model(self, models_root):
        model_path = os.path.join(models_root, self.model_path())
        if os.path.exists(model_path):
            self.model = models.load_model(model_path, compile=False)
            return True

        return False

    def save_model(self, models_root):
        self.model.save(os.path.join(models_root, self.model_path()))

    @classmethod
    def model_path(cls):
        return f"{cls.__name__}.h5"

    def build_model() -> models.Model:
        raise NotImplementedError
