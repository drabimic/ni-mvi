import glob
import os
import shutil
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--src-path', required=True)
    parser.add_argument('--dest-path', required=True)

    args = vars(parser.parse_args())

    test = dict()

    print("Iterating")
    for file_path in glob.iglob(os.path.join(args["src_path"], "**", f"*.jpg"), recursive=True):
        path, frame = os.path.split(file_path)
        path, _ = os.path.split(path)
        _, video_number = os.path.split(path)

        vid_frames = test.setdefault(video_number, dict())
        if frame not in vid_frames:
            vid_frames[frame] = file_path

    print("Copying")
    for vid_idx, frame_dict in test.items():
        vid_root = os.path.join(args["dest_path"], vid_idx)
        if not os.path.exists(vid_root):
            os.makedirs(vid_root)

        for frame_idx, file_path in frame_dict.items():
            shutil.copy2(file_path, os.path.join(vid_root, frame_idx))


if __name__ == "__main__":
    main()
