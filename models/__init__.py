from enum import Enum
import models.dcgan_model
import models.unet_model


class ModelTypes(Enum):
    UNET = 1
    DCGAN = 2


def initialize_model(model_type: ModelTypes, models_root="./checkpoints", training=False):
    model = None

    if model_type is ModelTypes.UNET:
        model = unet_model.UNet(models_root)
    elif model_type is ModelTypes.DCGAN:
        model = dcgan_model.DCGAN(models_root)
    else:
        print(model_type)
        raise NotImplementedError()

    if training:
        model.load_model_training()
    else:
        model.load_model()
    return model
